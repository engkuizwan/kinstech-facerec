/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import SafeAreaView from 'react-native-safe-area-view';


import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CameraScreen from './src/screens/CameraScreen'; // Update with your actual component path

const Stack = createStackNavigator();



function App(){
  // const isDarkMode = useColorScheme() === 'dark';



  return (
    <View style={styles.sectionContainer}>
      
      <Text>Face recogination is  here baby</Text>
       <CameraScreen />
      {/* <NavigationContainer>
      <Stack.Navigator initialRouteName="Camera">
        <Stack.Screen name="Camera" component={CameraScreen} /> */}

        {/* Add more screens/routes as needed */}
      {/* </Stack.Navigator>
    </NavigationContainer> */}
    </View> 
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    flex: 1,
    marginTop: 0,
    paddingHorizontal: 24,
    backgroundColor :'white'
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
